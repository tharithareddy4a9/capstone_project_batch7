package com.hcl.shopforhome.bean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Products")
public class ProductDetails {

	@Id
	private int productId;
	private String productName;
	private String productDescription;
	private String productImage;
	private float price;
	private long stocks;

	// Default Constructor
	public ProductDetails() {
		super();

	}

	// Parameterized Constructor
	public ProductDetails(int productId, String productName, String productDescription, String productImage,
			float price, long stock) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.productDescription = productDescription;
		this.productImage = productImage;
		this.price = price;
		this.stocks = stock;
	}

	// Getters and Setters
	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getProductImage() {
		return productImage;
	}

	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public long getStocks() {
		return stocks;
	}

	public void setStocks(long stocks) {
		this.stocks = stocks;
	}

	// toString() method
	@Override
	public String toString() {
		return "ProductDetails [productId=" + productId + ", productName=" + productName + ", productDescription="
				+ productDescription + ", productImage=" + productImage + ", price=" + price + ", stocks=" + stocks
				+ "]";
	}

}
