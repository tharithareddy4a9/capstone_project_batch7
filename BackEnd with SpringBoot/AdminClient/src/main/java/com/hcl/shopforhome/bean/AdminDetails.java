package com.hcl.shopforhome.bean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "adminDetails")
public class AdminDetails {

	@Id
	private String email;
	private String userName;
	private String password;

	// Setters and Getters
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	// toString() method
	@Override
	public String toString() {
		return "AdminDetails [email=" + email + ", userName=" + userName + ", password=" + password + "]";
	}

}
