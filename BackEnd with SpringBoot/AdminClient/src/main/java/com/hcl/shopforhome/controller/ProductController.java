package com.hcl.shopforhome.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.hcl.shopforhome.bean.ProductDetails;

@RestController
@RequestMapping("/ProductCRUDS")
@CrossOrigin
public class ProductController {

	@Autowired
	RestTemplate restTemplate;

	/**********************************
	 * Product Operations
	 ***********************************/

	// To get all the products
	@GetMapping(value = "getallprod")
	public List<Object> getAllProducts() {
		String Url = "http://user-service:8282/ProductCRUD/retrieveProducts";
		Object[] res = restTemplate.getForObject(Url, Object[].class);
		return Arrays.asList(res);
	}

	// To add the product
	@PostMapping(value = "storeprod", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeProd(@RequestBody Object obj) {
		String Url = "http://user-service:8282/ProductCRUD/createProduct/";
		ResponseEntity<String> user = restTemplate.postForEntity(Url, obj, String.class);
		return user.getBody();
	}

	// To update the product
	@PutMapping(value = "updateprod", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String updateProd(@RequestBody ProductDetails prod) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<ProductDetails> entity = new HttpEntity<>(prod, headers);
		String url = "http://user-service:8282/ProductCRUD/updateProduct";
		return restTemplate.exchange(url, HttpMethod.PUT, entity, String.class).getBody();

	}

	// To delete the product
	@DeleteMapping(value = "deleteprod/{id}")
	public String deleteProd(@PathVariable("id") int id) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<ProductDetails> entity = new HttpEntity<>(headers);
		String url = "http://user-service:8282/ProductCRUD/deleteProduct/" + id;
		return restTemplate.exchange(url, HttpMethod.DELETE, entity, String.class).getBody();

	}

}