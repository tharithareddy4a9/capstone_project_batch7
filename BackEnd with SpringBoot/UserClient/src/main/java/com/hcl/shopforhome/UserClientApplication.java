package com.hcl.shopforhome;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication(scanBasePackages = "com.hcl.shopforhome")
@EnableJpaRepositories(basePackages = "com.hcl.shopforhome.dao")
@EntityScan(basePackages = "com.hcl.shopforhome.bean")
@EnableEurekaClient
@EnableSwagger2
public class UserClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserClientApplication.class, args);
		System.out.println("Userclient running on port 8282 ");
	}

	@Bean // container will call this method when string boot load the application.
	public Docket api() {
		System.out.println("object created..");
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.basePackage("com"))
				.build();
	}
}
