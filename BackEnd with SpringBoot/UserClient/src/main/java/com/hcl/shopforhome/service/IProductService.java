package com.hcl.shopforhome.service;

import java.util.List;

import com.hcl.shopforhome.bean.Products;

public interface IProductService {

	public Products getProduct(int id);

	public String createProduct(Products prod);

	public List<Products> retrieveProducts();

	public String updateProduct(Products prod);

	public String deleteProduct(int id);
}
