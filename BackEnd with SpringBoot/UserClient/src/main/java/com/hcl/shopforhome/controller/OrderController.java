package com.hcl.shopforhome.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.hcl.shopforhome.bean.Order;
import com.hcl.shopforhome.bean.OrderProduct;
import com.hcl.shopforhome.bean.OrderProductDto;
import com.hcl.shopforhome.bean.OrderStatus;

import com.hcl.shopforhome.customexception.ResourceNotFoundException;

import com.hcl.shopforhome.service.OrderProductService;
import com.hcl.shopforhome.service.OrderService;
import com.hcl.shopforhome.service.ProductService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/orders")
public class OrderController {

	ProductService productService;

	OrderService orderService;

	OrderProductService orderProductService;

	public OrderController(ProductService productService, OrderService orderService,
			OrderProductService orderProductService) {
		this.productService = productService;
		this.orderService = orderService;
		this.orderProductService = orderProductService;

	}

	// To get all Orders
	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public @NotNull Iterable<Order> list() {
		return this.orderService.getAllOrders();
	}

	// To create order form
	@PostMapping
	public ResponseEntity<Order> create(@RequestBody OrderForm form) {
		List<OrderProductDto> formDtos = form.getProductOrders();
		validateProductsExistence(formDtos);
		Order order = new Order();
		order.setStatus(OrderStatus.PAID.name());
		order = this.orderService.create(order);

		List<OrderProduct> orderProducts = new ArrayList<>();
		for (OrderProductDto dto : formDtos) {
			orderProducts.add(orderProductService.create(new OrderProduct(order,
					productService.getProduct(dto.getProduct().getProductId()), dto.getQuantity())));
		}

		order.setOrderProducts(orderProducts);

		this.orderService.update(order);
		try {
			productService.StockDemandEmail();
		} catch (Exception e) {
			System.out.println(e);
		}

		String uri = ServletUriComponentsBuilder.fromCurrentServletMapping().path("/orders/{id}")
				.buildAndExpand(order.getId()).toString();
		HttpHeaders headers = new HttpHeaders();
		headers.add("Location", uri);

		return new ResponseEntity<>(order, headers, HttpStatus.CREATED);
	}

	private void validateProductsExistence(List<OrderProductDto> orderProducts) {
		List<OrderProductDto> list = orderProducts.stream()
				.filter(op -> Objects.isNull(productService.getProduct(op.getProduct().getProductId())))
				.collect(Collectors.toList());

		if (!CollectionUtils.isEmpty(list)) {
			new ResourceNotFoundException("Product not found");
		}

	}

	public static class OrderForm {

		private List<OrderProductDto> productOrders;

		public List<OrderProductDto> getProductOrders() {
			return productOrders;
		}

		public void setProductOrders(List<OrderProductDto> productOrders) {
			this.productOrders = productOrders;
		}

	}

//    public static void stockDemandEmail() throws AddressException,MessagingException,IOException{
//		
//				 Properties props = new Properties();
//				   props.put("mail.smtp.auth", "true");
//				   props.put("mail.smtp.starttls.enable", "true");
//				   props.put("mail.smtp.host", "smtp.hcl.com");
//				   props.put("mail.smtp.port", "587");
//
//				 Session session = Session.getInstance(props, new javax.mail.Authenticator() {
//				      protected PasswordAuthentication getPasswordAuthentication() {
//				         return new PasswordAuthentication("a.saravanan@hcl.com", "ShawnSarav#151");
//				      }
//				   });
//				   Message msg = new MimeMessage(session);
//				   msg.setFrom(new InternetAddress("a.saravanan@hcl.com", false));
//
//				   msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse("saravananshaw151@gmail.com"));
//				   msg.setSubject("Testing For Project from hcl machine");
//				   msg.setContent("Hello Admin, ", "text/html");
//				   msg.setSentDate(new Date());
//
//				   MimeBodyPart messageBodyPart = new MimeBodyPart();
//				   messageBodyPart.setContent("Stock needed in the product Id: ", "text/html");
//
//				   Multipart multipart = new MimeMultipart();
//				   multipart.addBodyPart(messageBodyPart);
//				  
//				   
//				   msg.setContent(multipart);
//				   Transport.send(msg);   
//
//                 
//			 }

}
