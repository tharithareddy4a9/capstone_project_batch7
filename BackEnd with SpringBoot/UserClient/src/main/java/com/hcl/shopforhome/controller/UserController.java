package com.hcl.shopforhome.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.shopforhome.bean.User;
import com.hcl.shopforhome.service.UserService;

@RestController
@RequestMapping("/UserActivity")
@CrossOrigin
public class UserController {

	@Autowired
	UserService userService;

	/**********************************
	 * User Operations
	 ***********************************/

	// Signup into User
	@PostMapping(value = "signUp", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String signUp(@RequestBody User user) {
		return userService.signUpUser(user);
	}

	// SignIn into User
	@PostMapping(value = "signIn", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String signIn(@RequestBody User user) {
		return userService.signInUser(user);
	}

	// Logout from User
	@GetMapping(value = "logout")
	public String logout() {
		return "Logged Out successfully";
	}

	// To add the user
	@PostMapping(value = "createUser", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String CreateUserInfo(@RequestBody User user) {
		return userService.createUser(user);
	}

	// To get the all users
	@GetMapping(value = "retrieveUsers", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<User> retrieveUserDetails() {
		return userService.retrieveUser();
	}

	// To update the user
	@PutMapping(value = "updateUser")
	public String updateUserdetails(@RequestBody User user) {
		return userService.updateUser(user);

	}

	// To delete the user
	@DeleteMapping(value = "deleteUser/{id}")
	public String deleteUserDetails(@PathVariable("id") int id) {
		return userService.deleteUser(id);
	}
}
