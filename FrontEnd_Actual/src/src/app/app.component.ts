import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Front_End_Actual';
  flag:boolean=true;
  inbound:boolean=true;
  constructor(public route:Router){}

  ChangeNav():void{
    this.flag = false;
    this.route.navigate(["useractivity"]);

  }
  ChangeNavAd():void{
    this.flag = false;
    this.route.navigate(["adminactivity"]);

  }
}
