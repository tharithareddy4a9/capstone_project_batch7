import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from '../product';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-user-homepage',
  templateUrl: './user-homepage.component.html',
  styleUrls: ['./user-homepage.component.css']
})
export class UserHomepageComponent implements OnInit {
   
  username:string="";

  products:Array<Product>=[];

  constructor(public route:Router,public ProdSer:ProductService) { }

  ngOnInit(): void {
    let res = sessionStorage.getItem("usname");
    if(res!=null){
      this.username=res;
    }
    this.loadProducts();
  }
  userlogout(){
    sessionStorage.removeItem("usname");
   this.route.navigate(["useractivity"])
  }

  addToCart(){
    this.route.navigate(["ecommerce"])
  }
  
  loadProducts(): void{
  
    this.ProdSer.getAllProducts().subscribe(res=>this.products=res);
  }

}
