import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import{HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminOperationComponent } from './admin-operation/admin-operation.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { UserOperationComponent } from './user-operation/user-operation.component';
import { AdminHomepageComponent } from './admin-homepage/admin-homepage.component';
import { UserHomepageComponent } from './user-homepage/user-homepage.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { EcommerceComponent } from './ecommerce/ecommerce.component';
import { OrdersComponent } from './ecommerce/orders/orders.component';
import { ProductsComponent } from './ecommerce/products/products.component';
import { ShoppingCartComponent } from './ecommerce/shopping-cart/shopping-cart.component';
import { SearchComponent } from './search/search.component';
import { SearchFilterPipe } from './SearchPipes/search-filter.pipe';
import { StockReportComponent } from './stock-report/stock-report.component'


@NgModule({
  declarations: [
    AppComponent,AdminOperationComponent, UserOperationComponent, AdminHomepageComponent, UserHomepageComponent, FileUploadComponent,
     EcommerceComponent,OrdersComponent,ProductsComponent, ShoppingCartComponent,SearchComponent, SearchFilterPipe, StockReportComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,ReactiveFormsModule,HttpClientModule,FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
